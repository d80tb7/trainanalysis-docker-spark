
Trainanalysis-Docker-Spark
========================

Simple docker image that contains spark-submit and a JDK. Idea is that this can be used as a base image for projects that are spark jobs so that they can be distibuted as a self-contained system.


This is based on the minimal [frolvlad/alpine-oraclejdk8](https://hub.docker.com/r/frolvlad/alpine-oraclejdk8/) which provides a Docker image with OracleJDK 8 in 167MB.

spark-sbumit.sh is located under /spark.



