
// project settings
name := "trainanalysis-spark-docker-base"

// Dependencies
libraryDependencies += "org.apache.spark" % "spark-core_2.11" % "1.6.1" 

// Merge Strategy for creating fat jar
assemblyMergeStrategy in assembly := {
  case x if x.endsWith(".class") => MergeStrategy.last
  case x if x.endsWith(".properties") => MergeStrategy.last
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    if (oldStrategy == MergeStrategy.deduplicate) MergeStrategy.first else oldStrategy(x)
}

enablePlugins(DockerPlugin)
mainClass in assembly := Some("org.apache.spark.deploy.SparkSubmit") 
imageNames in docker := Seq(ImageName(s"d80tb7/${name.value}:latest"))

dockerfile in docker := {
  // The assembly task generates a fat JAR file
  val artifact: File = assembly.value
  val artifactTargetPath = "/spark/spark-submit.jar"
  val launchScript = baseDirectory.value /"scripts/spark-submit.sh"

  new Dockerfile {
    from("frolvlad/alpine-oraclejdk8")
    add(artifact, artifactTargetPath)
    add(launchScript, "/spark/spark-submit.sh")
  }
}

